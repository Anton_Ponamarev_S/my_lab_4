import { concat } from "lodash";

export function join(array, separator = ',') {
    let joined = '';
    for (let i = 0; i < array.length;i++){
        if(i === array.length - 1) {joined += array[i]}
        else {joined += array[i] + separator}
    }
return joined;
}

export function remove(array, func) {
    let removed = [];
    for(let i = 0;i < array.length; i++){
        if (func(array[i])){
            removed.push(array[i]);
            array.splice(i, 1);
            i-=1;
        }
    }
    return removed;
}

export function union(...arrays) {
    const united = concat(...arrays);
    const united_set = new Set(united);
    const completed_union = Array.from(united_set);
    return completed_union;
}

export function pullAt(array, indexes) {
    let removed = new Array(indexes.length);
    if(!array.length) {
        return removed;
    }
    indexes.sort((s, t) => t - s);
    indexes.forEach((value, index) => removed.splice(index, 1, array.splice(value, 1)[0]));
    return removed;
}
export function flip(func) {
    return (...arg) => func(...arg.reverse());
}

export default {join, remove, union, pullAt, flip}